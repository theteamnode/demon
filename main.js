var express=require("express");
var app=express();
var bodyParser=require('body-parser');
var objectId=require('mongodb').ObjectID;
var MongoClient=require('mongodb').MongoClient;
var fs = require("fs");
var formidable = require('formidable');
const path = require('path');
var md5 = require('md5');

var urlencodeedParser=bodyParser.urlencoded({extended:false});
app.set('view engine', 'ejs');

var promiseconnect=require('./promisedata');
	

//app.use(express.static(path.join(__dirname, 'upload')));
app.use(express.static('public'));

var session = require('express-session');


app.use(session({
    
    name: '_es_demo',
    secret: '1234',
    resave: false,

    saveUninitialized: false
}));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded());


app.use(express.static('public'));
app.get('/log-in',function(req,res){
    if(req.session.email){
        //res.render(__dirname+'/'+'home',{"data": req.session.email});
        res.redirect('/home');
    }
    else{
        res.sendFile(__dirname+'/'+'login.html');
    }
    
   
})
app.get('/home',function(req,res){
    if(req.session.email){
        promiseconnect.auth('registration',{"email":req.session.email}).then((fromResolve)=>{
            
            if( req.session.message){
               // fromResolve.push({"message":req.session.message});

                res.render(__dirname+'/'+'home',{"data":fromResolve,"message":req.session.message });
            }
            else{
                console.log(fromResolve);
                res.render(__dirname+'/'+'home',{"data":fromResolve });
            }
                
            
            
        }).catch((error)=>{
            res.redirect('/log-in');
        })
        
    }
    else{
        
        res.redirect('/log-in');
    }
})

app.get('/sign-up',function(req,res){
    
    res.sendFile(__dirname+'/'+'signup.html');
})
app.post('/register',function(req,res){
   
    var encryptedString =md5(req.body.password);
    var today = new Date();
    promiseconnect.auth('registration',{"email":req.body.email}).then((fromResolve)=>{
        if(fromResolve.length>0)
        {
            res.send('1000');
          
        }
        else{
            promiseconnect.register('registration',{name:req.body.name,phone:req.body.phone,email:req.body.email,password:encryptedString,address:req.body.address,date:today}).then((Resolve)=>{
                res.send('1001');
               
          
              }).catch((error)=>{
                
                res.send('1003');
       
              });
          

        }
        
        

    }).catch((error)=>{
        res.send('1004');
      
    });
   
    
})

app.post('/login',function(req,res){
    var encryptedString = md5(req.body.password);
    
    req.session.email=req.body.email;
    promiseconnect.auth('registration',{"email":req.body.email,"password":encryptedString}).then((fromResolve)=>{
        if(fromResolve.length==1)
        {
            res.send('1000');
           
        }
        else{
            res.send('1001');
          
        }
        
        

    }).catch((error)=>{
        res.send('1002');
       
    });
})

app.post('/update',function(req,res){
    var form = new formidable.IncomingForm();
            form.parse(req, function (err, fields, files) {
                if(files.profile.name){
                    var oldpath = files.profile.path;
                var newpath = __dirname+'/upload/'+files.profile.name;
                fs.rename(oldpath, newpath, function (err) {
                    if (err) 
                    {
                    //     res.json({
                    //         "exception": error ,
                    //         "status": 400,
                    //         "error": true,
                    //         "message": "Somthing is wrong1",
                    //   });
                      req.session.message="error";
                    }
                    else{
                        promiseconnect.updateData('registration',{"email":fields.email}, {"name":fields.name,"address":fields.address,"profile":files.profile.name,"phone":fields.phone,"country":fields.country,"state":fields.state,"city":fields.city}).then((fromResolve)=>{
                            req.session.message="success";
                            
                            res.redirect('/home');
                    
                        }).catch((error)=>{
                            // res.json({
                            //         "exception": error ,
                            //         "status": 400,
                            //         "error": true,
                            //         "message": "Somthing is wrong",
                            // });
                            req.session.message="error";
                        });

                        
                    }
                    
                });

                }
                else{
                    promiseconnect.updateData('registration',{"email":fields.email}, {"name":fields.name,"address":fields.address,"phone":fields.phone,"country":fields.country,"state":fields.state,"city":fields.city}).then((fromResolve)=>{
                        req.session.message="success";
                        res.redirect('/home');
                
                    }).catch((error)=>{
                        // res.json({
                        //         "exception": error ,
                        //         "status": 400,
                        //         "error": true,
                        //         "message": "Somthing is wrong",
                        // });
                        req.session.message="error";
                    });

                }
                
            }); 
  
})

app.listen(8080,function(req,res){
    console.log("Project run in port 8080");
})
